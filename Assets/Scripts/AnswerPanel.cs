﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerPanel : MonoBehaviour
{
    public int answerIndex;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f))
            {
               
                if (hit.collider.gameObject == gameObject)
                {
                    QuestionsManager.Instance.CheckAnswer(answerIndex, GetComponent<MeshRenderer>());
                }
            }
        }
    }
}
