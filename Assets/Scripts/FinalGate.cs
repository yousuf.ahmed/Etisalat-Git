﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalGate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnCollisionStay(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.transform.CompareTag("Player"))
        {
            PuzzleManger.Instance.lastRoadSpeed = 0;
            RoadsManger.Instance.roadSpeed = 0;
            PlayerController.Instance.winningScene.enabled = true;
            PlayerController.Instance.winningScene.Play();
            GetComponent<Collider>().enabled = false;
        }
    }
}
