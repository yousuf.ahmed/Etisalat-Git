﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GateTitle : MonoBehaviour
{
    public Texture title;
    public MeshRenderer gateTitle;
    void Start()
    {
        
    }
    public void Apply()
    {
        gateTitle.material.mainTexture = title;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
