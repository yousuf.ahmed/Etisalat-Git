﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{

    public static ObjectPool SharedInstance;
    public List<GameObject> pooledObjects;
    public ObjectToPool[] objectToPool;
    public Transform pooledObjectsParent;
    void Awake()
    {
        SharedInstance = this;
    }

    void Start()
    {
        pooledObjects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < objectToPool.Length; i++)
        {
            for (int j = 0; j < objectToPool[i].ammount; j++)
            {
                tmp = Instantiate(objectToPool[i].gameObject);
                tmp.transform.parent = pooledObjectsParent;
                tmp.SetActive(false);
                pooledObjects.Add(tmp);
            }
        }

        ObjectPool.SharedInstance.GetPooledObject(0).SetActive(true);
        ObjectPool.SharedInstance.GetPooledObject(1).SetActive(true);
        ObjectPool.SharedInstance.GetPooledObject(2).SetActive(true);
        GameObject lastRoad = SharedInstance.GetPooledObject(6);
        lastRoad.SetActive(true);
        RoadsManger.Instance.lastRoad = lastRoad.transform;
    }
    public GameObject GetPooledObject(int index)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].GetComponent<Road>().index == index)
            {
                return pooledObjects[i];
            }
        }
        return null;
    }
}

[System.Serializable]
public class ObjectToPool
{
    public GameObject gameObject;
    public int ammount;
}

