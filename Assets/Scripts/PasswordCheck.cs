﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class PasswordCheck : MonoBehaviour
{
    public string password;
    public TMP_InputField inputField;
    void Start()
    {
        
    }
    public void CheckPassword()
    {
        if (inputField.text == password)
        {
            SceneManager.LoadScene(1);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
