﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public ParticleSystem particleSystem;
    public Transform leftStep, rightStep, centerStep;
    public float jumpForce = 2f, moveSpeed = 10;
    [HideInInspector] public Animator animator;
    Rigidbody rigidbody;
    CapsuleCollider capsuleCollider;
    [HideInInspector] public bool isGrounded;
    bool left, center = true, right;
    public bool sliding;
    public bool isDead;
    public bool gameStarted;
    public AudioClip jump, hit, slide;
    public PlayableDirector winningScene;
    public AudioSource audioSource, background;
    public GameObject deadUI;
    public GameObject pauseMenu, pauseButton;
    GameObject lastCollided;
    public AudioSource collectCoin;
    public Text score;
    public int coinsCollected;
    List<GameObject> hiddenObstcles = new List<GameObject>();
    public ParticleSystem coinLoss;
    #region Singelton
    private static PlayerController _instance;

    public static PlayerController Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion
    void Start()
    {
        isGrounded = true;
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Pause();
            pauseMenu.SetActive(true);
            pauseButton.SetActive(false);
        }
        if (rigidbody.IsSleeping())
            rigidbody.WakeUp();

        if (isDead == false)
        {
            if (left)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(leftStep.position.x, transform.position.y, transform.position.z), Time.deltaTime * moveSpeed);
            }
            if (right)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(rightStep.position.x, transform.position.y, transform.position.z), Time.deltaTime * moveSpeed);
            }
            if (center)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(centerStep.position.x, transform.position.y, transform.position.z), Time.deltaTime * moveSpeed);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                if (isGrounded && !sliding && !PuzzleManger.Instance.isQA)
                {
                    if (transform.position.x == 0f || transform.position.x == 4f)
                    {
                        particleSystem.Play();
                        animator.SetTrigger("RunLeft");
                        if (center)
                        {
                            left = true;
                            center = false;
                        }
                        if (right)
                        {
                            center = true;
                            right = false;
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                if (isGrounded && !sliding && !PuzzleManger.Instance.isQA)
                {
                    if (transform.position.x == 0f || transform.position.x == -4f)
                    {
                        particleSystem.Play();
                        animator.SetTrigger("RunRight");
                        if (center)
                        {
                            right = true;
                            center = false;
                        }
                        if (left)
                        {
                            center = true;
                            left = false;
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                Revive();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                if (isGrounded && !PuzzleManger.Instance.isQA)
                {
                    //rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Acceleration);
                    animator.SetTrigger("Jump");
                    audioSource.pitch = Random.Range(0.8f, 1f);
                    audioSource.PlayOneShot(jump);
                    //capsuleCollider.center = new Vector3(0, 1.813344f, 0);
                    //capsuleCollider.height = 1.456217f;
                    isGrounded = false;
                }
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                if (isGrounded && !PuzzleManger.Instance.isQA)
                {
                    if (transform.position.x == 0f || transform.position.x == -4f || transform.position.x == 4f)
                    {
                        audioSource.PlayOneShot(slide, 1f);
                        particleSystem.Play();
                        //capsuleCollider.center = new Vector3(0, 0.3693252f, 0);
                        //capsuleCollider.height = 0.6962066f;
                        animator.SetTrigger("Slide");
                        sliding = true;
                    }
                }
            }
        }


    }

    public void Pause()
    {
        Time.timeScale = 0;
    }
    public void Unpause()
    {
        Time.timeScale = 1;
    }
    public void StartGame()
    {
        gameStarted = true;
        animator.Play("run");
    }

    IEnumerator ReactiveObstcle()
    {
        yield return new WaitForSeconds(2f);
        foreach(GameObject obstcle in hiddenObstcles)
        {
            obstcle.SetActive(true);
        }
        lastCollided.SetActive(true);
    }
    public void Revive()
    {

        if (lastCollided)
        {
            lastCollided.SetActive(false);
            StartCoroutine(ReactiveObstcle());
        }

        foreach (GameObject _panel in QuestionsManager.Instance.answerPanels)
        {
            _panel.GetComponent<Collider>().enabled = true;
        }
        Camera.main.transform.position = PuzzleManger.Instance.cameraGamePosition.transform.position;
        Camera.main.transform.rotation = PuzzleManger.Instance.cameraGamePosition.transform.rotation;
        background.volume = 1f;
        PuzzleManger.Instance.isQA = false;
        QuestionsManager.Instance.chance = false;
        background.Play();
        animator.Play("run");
        PuzzleManger.Instance.lastRoadSpeed = 10;
        RoadsManger.Instance.roadSpeed = 10;
        RoadsManger.Instance.currentIndex = 1;
        isDead = false;
        isGrounded = true;
        //left = false;
        //right = false;
        //center = true;
        sliding = false;
        //transform.position = new Vector3(centerStep.transform.position.x, transform.position.y, transform.position.z);

        /*
        foreach(Transform road in ObjectPool.SharedInstance.pooledObjectsParent)
        {
            road.gameObject.SetActive(false);
        }
        
        GameObject one = ObjectPool.SharedInstance.GetPooledObject(0);
        one.SetActive(true);
        one.transform.position = new Vector3(0, 0, 40f);
        GameObject two = ObjectPool.SharedInstance.GetPooledObject(1);
        two.SetActive(true);
        two.transform.position = new Vector3(0, 0, 87.9f);
        GameObject three = ObjectPool.SharedInstance.GetPooledObject(2);
        three.SetActive(true);
        three.transform.position = new Vector3(0, 0, 135.8f);
        GameObject lastRoad = ObjectPool.SharedInstance.GetPooledObject(4);
        lastRoad.transform.position = new Vector3(0, 0, 183.7f);
        lastRoad.SetActive(true);
        RoadsManger.Instance.lastRoad = lastRoad.transform;
        */
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Obstcle"))
        {
            coinsCollected--;
            score.text = coinsCollected.ToString();
            coinLoss.Play();
            coinLoss.GetComponent<AudioSource>().Play();
            score.GetComponent<Animator>().Play("Play");
            lastCollided = collision.gameObject;
            hiddenObstcles.Add(lastCollided);
            audioSource.PlayOneShot(hit);
            background.Pause();
            PuzzleManger.Instance.lastRoadSpeed = 0;
            RoadsManger.Instance.roadSpeed = 0;
            animator.Play("Fall");
            deadUI.SetActive(true);
            isDead = true;
            Debug.Log("GameOver");
        }
        if (collision.transform.CompareTag("Coin"))
        {
            coinsCollected++;
            score.text = coinsCollected.ToString();
            score.GetComponent<Animator>().Play("Play");
            collision.gameObject.SetActive(false);

            collectCoin.Play();
        }
        if (collision.transform.CompareTag("ObstcleHard"))
        {
            lastCollided = collision.transform.parent.gameObject;
            hiddenObstcles.Add(lastCollided);
            audioSource.PlayOneShot(hit);
            background.Pause();
            PuzzleManger.Instance.lastRoadSpeed = 0;
            RoadsManger.Instance.roadSpeed = 0;
            animator.Play("Fall");
            deadUI.SetActive(true);
            isDead = true;
            Debug.Log("GameOver");
        }
    }
    public void ChangeColliderHeight()
    {
        //capsuleCollider.center = new Vector3(0, 0.9731109f, 0);
        //capsuleCollider.height = 1.903778f;
        sliding = false;
        isGrounded = true;
    }
    public void Landing()
    {
        //capsuleCollider.center = new Vector3(0, 0.9731109f, 0);
        //capsuleCollider.height = 1.903778f;
        isGrounded = true;
    }
    /*
    private void OnCollisionStay(Collision collision)
    {
        animator.SetBool("JumpDown", false);
        animator.SetBool("Grounded", true);
        isGrounded = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        animator.SetBool("Grounded", false);
        isGrounded = false;
    }*/
}
