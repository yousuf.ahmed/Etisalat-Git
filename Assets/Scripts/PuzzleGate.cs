﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGate : MonoBehaviour
{
    public int questionID;
    public Texture title;
    public MeshRenderer gateTitle;
    void OnEnable()
    {
        if (PlayerController.Instance.gameStarted)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            GetComponent<Collider>().enabled = true;
            questionID = QuestionsManager.Instance.currentQuestion;
            gateTitle.material.mainTexture = QuestionsManager.Instance.gateTitles[QuestionsManager.Instance.currentTitle];
            QuestionsManager.Instance.gateIcon.sprite = QuestionsManager.Instance.gateIcons[QuestionsManager.Instance.currentTitle];
        }
    }
    public void Apply()
    {
        gateTitle.material.mainTexture = QuestionsManager.Instance.gateTitles[QuestionsManager.Instance.currentTitle];
        QuestionsManager.Instance.gateIcon.sprite = QuestionsManager.Instance.gateIcons[QuestionsManager.Instance.currentTitle];

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionStay(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.transform.CompareTag("Player"))
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

            PuzzleManger.Instance.StartPuzzle();
            GetComponent<Collider>().enabled = false;
        }
    }
}
