﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PuzzleManger : MonoBehaviour
{
    public Transform cameraQAposition, cameraGamePosition, cameraDeadPosition;
    public float cameraMovmentSpeed, cameraAngularSpeed;
    public GameObject qaPanel;
    public TextMeshPro QuestionHeaderText;
    public TextMeshPro[] QuestionChoicesTexts;
    public float lastRoadSpeed;
    Camera camera;
     public bool isQA;
    #region Singelton
    private static PuzzleManger _instance;

    public static PuzzleManger Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion
    void Start()
    {
        lastRoadSpeed = RoadsManger.Instance.roadSpeed;
        camera = Camera.main;
    }

    public void StartPuzzle()
    {
        QuestionsManager.Instance.SetQuestion();
        StartQA(true);
    }
    public void StartQA(bool start)
    {
        if (start)
        {
            lastRoadSpeed = RoadsManger.Instance.roadSpeed;
            PlayerController.Instance.animator.Play("Idle");
            isQA = true;
        }
        else
        {

            foreach (TextMeshPro choice in QuestionChoicesTexts)
            {
                choice.transform.parent.gameObject.SetActive(false);
            }
            PlayerController.Instance.animator.Play("run");
            isQA = false;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (!isQA)
            {
                StartQA(true);
            }
            else
            {
                StartQA(false);
            }
        }
        if (isQA)
        {
            
            qaPanel.SetActive(true);
            RoadsManger.Instance.roadSpeed = 0;
            camera.transform.position = Vector3.Lerp(camera.transform.position, cameraQAposition.transform.position, Time.deltaTime * cameraMovmentSpeed);
            camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, cameraQAposition.transform.rotation, Time.deltaTime * cameraAngularSpeed);
        }
        else
        {
            qaPanel.SetActive(false);
            
            RoadsManger.Instance.roadSpeed = lastRoadSpeed;
            if (PlayerController.Instance.isDead)
            {
                camera.transform.position = Vector3.Lerp(camera.transform.position, cameraDeadPosition.transform.position, Time.deltaTime * (cameraMovmentSpeed/2));
                camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, cameraDeadPosition.transform.rotation, Time.deltaTime * (cameraAngularSpeed/2));

            }
            else
            {
                camera.transform.position = Vector3.Lerp(camera.transform.position, cameraGamePosition.transform.position, Time.deltaTime * cameraMovmentSpeed);
                camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, cameraGamePosition.transform.rotation, Time.deltaTime * cameraAngularSpeed);
            }
        }
    }
}
