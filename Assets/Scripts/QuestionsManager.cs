using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionsManager : MonoBehaviour
{
    public AudioClip right, wrong;
    [HideInInspector] public bool chance;
    public Animator wronngAnswer, transition;
    public AudioSource audioSource, backgroundMusic;
    public int currentRightAnswer;
    public int currentQuestion, currentTitle;
    public SpriteRenderer gateIcon;
    public List<Texture> gateTitles = new List<Texture>();
    public List<Sprite> gateIcons = new List<Sprite>();
    [Header("Managers")]
    public List<Question> Questions;
    public List<GameObject> answerPanels;
    #region Singelton
    private static QuestionsManager _instance;

    public static QuestionsManager Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion
    #region Methods
    public void SetQuestion()
    {
        //currentQuestion = questionIndex;
        //Set Question Header.
        backgroundMusic.volume = 0.2f;
        PuzzleManger.Instance.QuestionHeaderText.text = Questions[currentQuestion].QuestionHead;
        currentRightAnswer = Questions[currentQuestion].answerIndex;

        //Set Question Choices.
        for (int i = 0; i < Questions[currentQuestion].QuestionChoices.Length; i++)
        {
            PuzzleManger.Instance.QuestionChoicesTexts[i].transform.parent.gameObject.SetActive(true);
            PuzzleManger.Instance.QuestionChoicesTexts[i].text = Questions[currentQuestion].QuestionChoices[i];
        }
    }
    public void SetAttachedQuestion(int attachedQuestion)
    {
        //currentQuestion = questionIndex;
        //Set Question Header.
        PuzzleManger.Instance.QuestionHeaderText.text = Questions[attachedQuestion].QuestionHead;
        currentRightAnswer = Questions[attachedQuestion].answerIndex;
        //Set Question Choices.
        for (int i = 0; i < Questions[attachedQuestion].QuestionChoices.Length; i++)
        {
            PuzzleManger.Instance.QuestionChoicesTexts[i].transform.parent.gameObject.SetActive(true);
            PuzzleManger.Instance.QuestionChoicesTexts[i].text = Questions[attachedQuestion].QuestionChoices[i];
        }
    }
    public void CheckAnswer(int _answerIndex, MeshRenderer panel)
    {
        if (_answerIndex == currentRightAnswer)
        {
            PlayerController.Instance.isGrounded = true;
            Debug.Log("Right");
            audioSource.PlayOneShot(right);
            panel.material.color = Color.green;
            foreach (GameObject _panel in answerPanels)
            {
                _panel.GetComponent<Collider>().enabled = false;
            }
            StartCoroutine(RightQuestionPit(panel));
        }
        else
        {
            PlayerController.Instance.coinsCollected--;
            PlayerController.Instance.coinLoss.Play();
            PlayerController.Instance.coinLoss.GetComponent<AudioSource>().Play();
            PlayerController.Instance.score.text = PlayerController.Instance.coinsCollected.ToString();
            PlayerController.Instance.score.GetComponent<Animator>().Play("Play");
            if (chance == false)
            {
                chance = true;
            }
            else
            {
                foreach (GameObject _panel in answerPanels)
                {
                    _panel.GetComponent<Collider>().enabled = false;
                }
                StartCoroutine(DeadPit(panel));             
            }
            audioSource.PlayOneShot(wrong);
            if (chance)
                wronngAnswer.Play("Pop");
            Debug.Log("false");
            panel.material.color = Color.red;
        }
    }
    #endregion
    IEnumerator DeadPit(MeshRenderer panel)
    {
        yield return new WaitForSeconds(1.5f);
        currentQuestion--;
        transition.Play("Revive");
    }
    IEnumerator RightQuestionPit(MeshRenderer panel)
    {
        yield return new WaitForSeconds(1.5f);
        foreach (GameObject _panel in answerPanels)
        {
            _panel.GetComponent<MeshRenderer>().material.color = Color.white;
            _panel.GetComponent<Collider>().enabled = true;
        }
        if (Questions[currentQuestion].attachedQuestion != 0 /*&& Questions[currentQuestion].attachedQuestionDone == false*/)
        {
            currentQuestion = Questions[currentQuestion].attachedQuestion;
            currentTitle = Questions[currentQuestion].questionTitle;
            foreach (PuzzleGate gate in FindObjectsOfType<PuzzleGate>())
            {
                if (gate.gameObject.activeInHierarchy)
                {
                    gate.Apply();
                }
            }
            SetQuestion();
            //SetAttachedQuestion(Questions[currentQuestion].attachedQuestion);
            //Questions[currentQuestion].attachedQuestionDone = true;
        }
        else
        {
            backgroundMusic.volume = 1f;
            chance = false;
            PuzzleManger.Instance.StartQA(false);
            PlayerController.Instance.isGrounded = true;
            PlayerController.Instance.sliding = false;
        }
    }
}

#region Question Structure
[System.Serializable]
public class Question
{
    public int questionTitle;
    public string QuestionHead;
    public string[] QuestionChoices;
    public int answerIndex;
    public int attachedQuestion;
    public bool attachedQuestionDone;
    public Question(string questionHead, string[] questionChoices, int _answerIndex, int _attachedQuestion, bool _attachedQuestionDone)
    {
        QuestionHead = questionHead;
        QuestionChoices = questionChoices;
        answerIndex = _answerIndex;
        attachedQuestion = _attachedQuestion;
        attachedQuestionDone = _attachedQuestionDone;
    }
    
}
#endregion

