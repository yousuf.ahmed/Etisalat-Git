﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public int index;
    public bool hard;
    public GameObject hardObsctles;
    public Transform coins;
    void Start()
    {
        
    }
    private void OnEnable()
    {
        if (QuestionsManager.Instance)
        {
            if (QuestionsManager.Instance.currentQuestion > 14)
                if (hardObsctles)
                    hardObsctles.SetActive(true);
                else
                    if (hardObsctles)
                    hardObsctles.SetActive(false);
        }
        if (coins)
        foreach (Transform child in coins) {
            child.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerController.Instance.gameStarted)
        {
            if (gameObject.activeInHierarchy)
            {
                transform.position -= new Vector3(0, 0, RoadsManger.Instance.roadSpeed * Time.deltaTime);
            }
            if (transform.position.z < -10f)
            {
                if (RoadsManger.Instance.currentIndex != 3)
                {
                    RoadsManger.Instance.GenerateNewRoad(Random.Range(0, 6));
                    RoadsManger.Instance.currentIndex++;
                }
                else
                {
                    if (QuestionsManager.Instance.currentQuestion < QuestionsManager.Instance.Questions.Count -1 )
                    {
                        RoadsManger.Instance.GenerateNewRoad(6);
                        RoadsManger.Instance.currentIndex = 0;
                        QuestionsManager.Instance.currentQuestion++;
                        QuestionsManager.Instance.currentTitle = QuestionsManager.Instance.Questions[QuestionsManager.Instance.currentQuestion].questionTitle;
                        foreach (PuzzleGate gate in FindObjectsOfType<PuzzleGate>())
                        {
                            if (gate.gameObject.activeInHierarchy)
                            {
                                gate.Apply();
                            }
                        }
                    }
                    else
                    {
                        RoadsManger.Instance.GenerateNewRoad(7);
                    }
                }

                gameObject.SetActive(false);
            }
        }
    }
}
