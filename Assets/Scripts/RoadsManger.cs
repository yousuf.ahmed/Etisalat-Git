﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadsManger : MonoBehaviour
{
    public int currentIndex = 2;
    public Transform lastRoad;
    public float roadSpeed;
    #region Singelton
    private static RoadsManger _instance;

    public static RoadsManger Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion
    void Start()
    {

    }

    public void GenerateNewRoad(int index)
    {
        GameObject _road = ObjectPool.SharedInstance.GetPooledObject(index);
        if (_road)
        {
            _road.transform.position = lastRoad.position + new Vector3(0, 0, 47f);
            _road.SetActive(true);
            lastRoad = _road.transform;
        }

    }
    // Update is called once per frame
    void Update()
    {
        PlayerController.Instance.animator.speed = roadSpeed / 100 + 1;
    }
}
