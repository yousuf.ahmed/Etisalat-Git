﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRevive : MonoBehaviour
{
    public void Revive()
    {
        PlayerController.Instance.Revive();
    }
}
